package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
//import org.testng.Reporter;
import org.testng.Reporter;

/**
 * @author Mahantesh
 *
 */
public class HomePage {
	WebDriver driver;
	
	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	/*public HomePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}*/
	
	@FindBy(how = How.XPATH, using="//a[@class='profilePic']")
	WebElement profileMenu;
	
	@FindBy(how = How.XPATH, using="//a[@id='btnSignOut']")
	WebElement signOutMenu;
	
	@FindBy(how = How.XPATH, using="//a[contains(text(),'Manage Account Details')]")
	WebElement manageAccountDetailsMenu;
	

	public void clickOnProfileMenu()
	{
		profileMenu.click();
	}
	
	public void clickOnSignOutLink()
	{
		signOutMenu.click();
	}
	
	public void clickOnManageAccountDetailsLink()
	{
		manageAccountDetailsMenu.click();
	}
		
	
}
