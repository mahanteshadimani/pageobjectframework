package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	//Declare webElements
	@FindBy(id="identifierId")
	private WebElement UniqueIdTextBox;

	@FindBy(id="upass")
	private WebElement passwordTextBox;

	@FindBy(id="btnSubmit")
	private WebElement SignInButton;

	//Initialization of webElements
	public LoginPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

	//Utilization of user
	public void setUniqueId(String Customer_uniqueId){
		UniqueIdTextBox.sendKeys(Customer_uniqueId);
	}

	public void setPassword(String Customer_password){
		passwordTextBox.sendKeys(Customer_password);
	}

	public void clickSignInButton(){
		SignInButton.click();
	}

}
