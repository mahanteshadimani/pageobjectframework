package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import utils.BrowserSetup;
import utils.CommonSteps;
//import utils.Log;

public class verifyProfilePageNavigation extends BrowserSetup{
	
	
	@Test(dataProvider="logindata")
	  public void LogIn(String uID,String pass) throws InterruptedException
	  {
		  LoginPage lPage=new LoginPage(driver);
		  //lPage.openLoginPage(super.baseUrl);
		  lPage.setUniqueId(uID);
		 // Log.info("Entered Username is "+ uID );
		  lPage.setPassword(pass);
		 // Log.info("Entered Password is "+ pass );
		  lPage.clickSignInButton();
		//  Log.info("Clicked on SignIn button");
	  }
  
  @Test
  public void profileNavigation() throws InterruptedException
  {
	  HomePage hPage=new HomePage(driver);
	  hPage.clickOnProfileMenu();
	  Thread.sleep(2000);
	  hPage.clickOnManageAccountDetailsLink();
  }
  
  @DataProvider(name="logindata")
  public Object[][] TestDataFeed(){
  Object [][] userdata=new Object[1][2];

  // Enter data to row 0 column 0
  userdata[0][0]="56678911";
  
  // Enter data to row 0 column 1
  userdata[0][1]="Admin@123";
  return userdata;
  }
}
