package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import utils.BrowserSetup;
import utils.CommonSteps;

public class verifyLoginFunctionality extends BrowserSetup{
//  WebDriver driver;
	//Logger log=Logger.getLogger(verifyLoginFunctionality.class);
  @Test(dataProvider="logindata")
  public void LogIn(String uID,String pass) throws InterruptedException
  {
	  LoginPage lPage=new LoginPage(driver);
	 // Reporter.log("Reporter log started",true);
	 // PropertyConfigurator.configure("log4j.properties");
      Reporter.log("Reporter log started",true);
      Reporter.log("Login was successful. Moving to next step",true);

	  lPage.setUniqueId(uID);
	  lPage.setPassword(pass);
	  lPage.clickSignInButton();

  }
  @Test
  public void LogOut() throws InterruptedException
  {
	  HomePage hPage=new HomePage(driver);
	  hPage.clickOnProfileMenu();
	  Thread.sleep(2000);
	  hPage.clickOnSignOutLink();
	  Thread.sleep(2000);
  }
  
  @DataProvider(name="logindata")
  public Object[][] TestDataFeed(){
  Object [][] userdata=new Object[2][2];

  // Enter data to row 0 column 0
  userdata[0][0]="56678911";
  
  // Enter data to row 0 column 1
  userdata[0][1]="Admin@123";
  
  userdata[1][0]="2ndUsername";
  userdata[1][1]="2ndPassword";
  return userdata;
  }
  
  @DataProvider(name="logindata1")
  public Object[][] TestDataFeed1(){
  Object [][] userdata=new Object[1][2];

  // Enter data to row 0 column 0
  userdata[0][0]="56678911";
  
  // Enter data to row 0 column 1
  userdata[0][1]="Admin@123";
  return userdata;
  }
}
